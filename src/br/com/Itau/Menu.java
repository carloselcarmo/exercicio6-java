package br.com.Itau;

import java.util.Map;

public class Menu
{
    Elevador elevador;

    public Menu()
    {
        elevador = new Elevador();
        this.elevador.inicializa(4, 10);
    }


    public void exibirOpcoesElevador()
    {
        Integer opcao;
        do
        {
            Map<String, Integer> opcoesJogo = IO.solicitarOpcao();
            opcao = opcoesJogo.get("opcao");

            if(opcao == 1)
            {
                elevador.entra();
            }
            else if(opcao == 2)
            {
                elevador.sai();
            }
            else if(opcao == 3)
            {
                elevador.sobe();
            }
            else if(opcao == 4)
            {
                elevador.desce();
            }
            else if(opcao == 5)
            {
                elevador.exibirMomentoAtualElevador();
            }

        } while(opcao != 6);
    }
}
