package br.com.Itau;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class IO
{
    public static void exibirMensagemPessoaEntrou ()
    {
        System.out.println("Uma pessoa entrou no elevador");
    }

    public static void exibirMensagemPessoaNaoEntrou ()
    {
        System.out.println("Elevador lotado! A pessoa não conseguiu entrar");
    }

    public static void exibirMensagemPessoaSaiu ()
    {
        System.out.println("Uma pessoa saiu do elevador");
    }

    public static void exibirMensagemNaoHaPessoasNoElevador ()
    {
        System.out.println("O elevador está vazio!");
    }

    public static void exibirMensagemElevadorSubiu ()
    {
        System.out.println("O elevador subiu um andar");
    }

    public static void exibirMensagemElevadorEstaNoTopo ()
    {
        System.out.println("O elevador já está no topo!");
    }

    public static void exibirMensagemElevadorDesceu ()
    {
        System.out.println("O elevador desceu um andar");
    }

    public static void exibirMensagemElevadorEstaNoTerreo ()
    {
        System.out.println("O elevador já está no térreo!");
    }

    public static void exibirMensagemMomentoAtualElevador(Integer andarAtual, Integer quantidadePessoasNoElevador)
    {
       String andar =  ( andarAtual == 0 ? "térreo" : andarAtual.toString());
       System.out.println("O elevador está no andar: " + andar + " e " + quantidadePessoasNoElevador + " pessoa(s) está(ão) nele");
    }

    public static Map<String, Integer> solicitarOpcao()
    {
        Scanner scanner = new Scanner(System.in);

        System.out.printf("Elevador -> ");
        System.out.printf("O que você deseja fazer? ");
        System.out.printf("(1) Colocar pessoa no elevador");
        System.out.printf("(2) Retirar pessoa do elevador");
        System.out.printf("(3) Subir andar");
        System.out.printf("(4) Descer andar");
        System.out.printf("(5) Mostrar elevador");
        System.out.printf("(6) Sair");
        Integer opcao = scanner.nextInt();

        Map<String, Integer> dados = new HashMap<>();
        dados.put("opcao", opcao);

        return dados;
    }
}
