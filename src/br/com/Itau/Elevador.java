package br.com.Itau;

public class Elevador
{
    Integer totalAndares;
    Integer quantidadePessoas;

    Integer andarAtual;
    Integer quantidadePessoasNoElevador;

    public Integer getTotalAndares()
    {
        return totalAndares;
    }

    public void setTotalAndares(Integer totalAndares)
    {
        this.totalAndares = totalAndares;
    }

    public Integer getQuantidadePessoas()
    {
        return quantidadePessoas;
    }

    public void setQuantidadePessoas(Integer quantidadePessoas)
    {
        this.quantidadePessoas = quantidadePessoas;
    }

    public Integer getAndarAtual()
    {
        return andarAtual;
    }

    public void setAndarAtual(Integer andarAtual)
    {
        this.andarAtual = andarAtual;
    }

    public Integer getQuantidadePessoasNoElevador()
    {
        return quantidadePessoasNoElevador;
    }

    public void setQuantidadePessoasNoElevador(Integer quantidadePessoasNoElevador)
    {
        this.quantidadePessoasNoElevador = quantidadePessoasNoElevador;
    }

    public void inicializa(Integer quantidadePessoas, Integer totalAndares)
    {
        this.quantidadePessoas = quantidadePessoas;
        this.totalAndares = totalAndares;

        andarAtual = 0;
        quantidadePessoasNoElevador = 0;
    }

    public void entra()
    {
        if(quantidadePessoasNoElevador + 1 <= quantidadePessoas)
        {
            quantidadePessoasNoElevador++;
            IO.exibirMensagemPessoaEntrou();
        }
        else
            IO.exibirMensagemPessoaNaoEntrou();
    }

    public void sai()
    {
        if(quantidadePessoasNoElevador > 0)
        {
            quantidadePessoasNoElevador--;
            IO.exibirMensagemPessoaSaiu();
        }
        else
            IO.exibirMensagemNaoHaPessoasNoElevador();
    }

    public void sobe()
    {
        if(andarAtual + 1 <= totalAndares)
        {
            andarAtual++;
            IO.exibirMensagemElevadorSubiu();
        }
        else
            IO.exibirMensagemElevadorEstaNoTopo();
    }

    public void desce()
    {
        if(andarAtual > 0)
        {
            andarAtual--;
            IO.exibirMensagemElevadorDesceu();
        }
        else
            IO.exibirMensagemElevadorEstaNoTerreo();
    }

    public void exibirMomentoAtualElevador()
    {
        IO.exibirMensagemMomentoAtualElevador(andarAtual, quantidadePessoasNoElevador);
    }
}
